apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "exchange.name" . }}
  labels:
    {{- include "exchange.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{ end }}
  selector:
    matchLabels:
      {{- include "exchange.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      annotations:
        {{- with .Values.podAnnotations }}      
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}    
      labels:
        {{- include "exchange.selectorLabels" . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/exchange/service:{{ .Values.images.service }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "exchange.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
