{{- $name := include "deploy.name" . }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "deploy.name" . }}
  labels:
    {{- include "deploy.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "deploy.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "deploy.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: dumper
          image: "{{ .Values.global.image.repository }}/elma365/deploy/service:{{ .Values.images.dumper }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          command:
            # Запуск приложения в режиме дампера
            - /srv/bin/deploy
            - start-dumper
          ports:
            - name: http-dumper
              containerPort: {{ .Values.global.containerPorts.dumper.http_port }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          env:
            - name: ELMA365_HTTP_BIND
              value: ":{{ .Values.global.containerPorts.dumper.http_port }}"
            - name: ELMA365_DUMP_TIMEOUT
              value: {{ .Values.appconfig.dumpTimeout }}
          volumeMounts:
            - name: dumps
              mountPath: /tmp/dumps
        {{- with .Values.dumperlivenessProbe }}
          livenessProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- with .Values.dumperreadinessProbe }}
          readinessProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- with .Values.dumperstartupProbe }}
          startupProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/deploy/service:{{ .Values.images.service }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          command:
            # Запуск приложения в режиме деплоера
            - /srv/bin/deploy
            - start-deploy
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.containerPorts.deploy.http_port }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: ELMA365_DEFAULT_CLUSTER_ID
              value: {{ .Values.appconfig.defaultClusterID }}
            - name: ELMA365_HTTP_CLIENT_TIMEOUT
              value: {{ .Values.appconfig.httpClientTimeout }}
            - name: ELMA365_ACTIVATE_TIMEOUT
              value: {{ .Values.appconfig.activateTimeout }}
            - name: ELMA365_MIGRATE_TIMEOUT
              value: {{ .Values.appconfig.migrateTimeout }}
            - name: ELMA365_DEFAULT_CONFIGURATION
              value: {{ .Values.appconfig.elma365config.defaultConfiguration | quote }}
            - name: ELMA365_DEMO_CONFIGURATION_BUCKET_NAME
              value: {{ .Values.appconfig.demoConfigurationBucketName | quote }}
            - name: ELMA365_TASK_LIVE_CHECK_PERIOD
              value: {{ .Values.appconfig.task.liveCheckPeriod }}
            - name: ELMA365_TASK_CANCELING_CHECK_PERIOD
              value: {{ .Values.appconfig.task.cancelingCheckPeriod }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          volumeMounts:
            - name: dumps
              mountPath: /tmp/dumps
        {{- with .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- with .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- with .Values.startupProbe }}
          startupProbe:
          {{- toYaml . | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
      volumes:
        - name: dumps
          emptyDir: {}
      terminationGracePeriodSeconds: 180
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "deploy.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
